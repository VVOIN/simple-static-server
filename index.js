'use strict';

const Hapi = require('hapi');

const server = new Hapi.Server({
	app: {
		host: process.env.SSS_HOST || '0.0.0.0',
		port: process.env.SSS_PORT || 8080
	}
});
const plugins = [
	{
		register: require('inert'),
	},
	{
		register: require('good'),
		options: {
			reporters: {
				console: [{
					module: 'good-squeeze',
					name: 'Squeeze',
					args: [{
						response: '*',
						log: '*'
					}]
				}, {
					module: 'good-console'
				}, 'stdout']
			}
		}
	}
];

server.connection({
	host: server.settings.app.host,
	port: server.settings.app.port
});
server.register(plugins, (err) => {
	if (err) throw err;

	server.route({
		method: 'GET',
		path: '/{param*}',
		handler: {
			directory: {
				path: 'files',
				listing: true
			}
		}
	});

	server.start((err) => {
		if (err) throw err;
		server.log('info', `Server running at: ${server.info.uri}`);
	});
});
