# Simple Static NodeJS Server


### Running server

```
npm install
node index.js
```


### Env vars

| var      | Meaning     | Default value |
|----------|-------------|---------------|
| SSS_HOST | server host | 0.0.0.0       |
| SSS_PORT | server port | 8080          |



### Usage

Put file in 'files' dir